#!/bin/sh

AMBIENTE_TESTE_CONFIGURADO=False

REPO=https://gitlab.com/manobray/conchayoro.git
NODE1=ip172-18-0-33-blk52mad7o0g00a8q410@direct.labs.play-with-docker.com
NODE2=ip172-18-0-46-blk52mad7o0g00a8q410@direct.labs.play-with-docker.com
NODE3=ip172-18-0-49-blk52mad7o0g00a8q410@direct.labs.play-with-docker.com
NODE4=ip172-18-0-57-blk52mad7o0g00a8q410@direct.labs.play-with-docker.com

if [ -z "$1" ]; then

  HOST_LOCAL=False

else

  HOST_LOCAL=True

fi

if [ $HOST_LOCAL = True ]; then

  echo "Configuração do ambiente local"
  
  docker-compose --compatibility up -d jenkins nexus sonar wildfly mysql

else

  echo "Configuração do ambiente remoto"
  
  ssh -t $NODE1 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d jenkins"

  ssh -t $NODE2 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d nexus"

  ssh -t $NODE3 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d sonar"

  ssh -t $NODE4 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d wildfly mysql" 

  if [ $AMBIENTE_TESTE_CONFIGURADO = True ];then

     ssh -t $NODE5@$HOST "git clone $REPO && cd conchayoro/ambiente && docker-compose -f docker-compose-teste.yml --compatibility up -d wildfly mysql"

  fi
   
  echo "Configuração do ambiente remoto concluída com sucesso!!"

fi
